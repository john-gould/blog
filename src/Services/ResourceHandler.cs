namespace Jdev.JohnGould.Blog.Grpc.Api.Services;

using System.Reflection;
using Interfaces;

public class ResourceHandler : IResourceHandler
{
    public async Task<string> ReadAllAsync(string name)
    {
        var assembly = Assembly.GetExecutingAssembly();
        var x = $"{assembly.GetName().Name}.Blogs.{name}.md";
        using var stream = assembly.GetManifestResourceStream(x);
        using var sr = new StreamReader(stream!);
        return await sr.ReadToEndAsync();
    }
}