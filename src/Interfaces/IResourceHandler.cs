namespace Jdev.JohnGould.Blog.Grpc.Api.Interfaces;

public interface IResourceHandler
{
    Task<string> ReadAllAsync(string name);
}