using Jdev.JohnGould.Blog.Grpc.Api;
using Jdev.JohnGould.Blog.Grpc.Api.Interfaces;
using Jdev.JohnGould.Blog.Grpc.Api.Services;
using Jdev.MDParser.Engine.Core;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddGrpc();
builder.Services.AddMarkDown();
builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddSingleton<IResourceHandler, ResourceHandler>();
builder.Services.AddGrpcReflection();

var app = builder.Build();

app.MapGrpcReflectionService();
app.MapGrpcService<MarkdownService>();
app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();