using Grpc.Core;

namespace Jdev.JohnGould.Blog.Grpc.Api;

using AutoMapper;
using Interfaces;
using System.Text.Json;
using Components.Models;
using MDParser.Engine.Interfaces;
using Jdev.JohnGould.Proto.Blog.Grpc.Markdown;
using static Jdev.JohnGould.Proto.Blog.Grpc.Markdown.MarkdownService;

public class MarkdownService : MarkdownServiceBase
{
    private readonly IMarkDown _markDown;
    private readonly IMapper _mapper;
    private readonly IResourceHandler _resourceHandler;
    
    public MarkdownService(IMarkDown markDown, IResourceHandler resourceHandler, IMapper mapper)
    {
        _markDown = markDown;
        _resourceHandler = resourceHandler;
        _mapper = mapper;
    }
    
    public override async Task<GetJsonResponse> GetJson(GetJsonRequest request, ServerCallContext context)
    {
        var raw = await _resourceHandler.ReadAllAsync(request.Name);
        var children = _mapper.Map<IEnumerable<BlogChildBase>>(await _markDown.GetContentsAsync(raw));
        var json = JsonSerializer.Serialize(children);
        return new()
        {
            Json = json
        };
    }
}