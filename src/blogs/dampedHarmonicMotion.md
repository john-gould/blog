# Damped Harmonic $x^2$ Motion
## Introduction

The equation in question is
\[
\ddot{x} + 2\beta\dot{x} + \omega_0^2x = 0\,.
\]
It describes scenarios where the restoring force is proportional to negative the displacement and there is a resistive force proportional to the velocity. Think of a mass on a spring subjected to air resistance. The initial conditions will be as generic as possible: $x(0)=X_0$ and $\dot{x}(0)=v$ with $X_0$ and $v$ both real (obvious but important for later).

With an ordinary, homogeneous differential equation of this sort we simply guess the general solution:

\[
x(t) = A \textrm{e}^{st}\qquad,\qquad A, s \in \mathbb{C}
\]

Substituting this into $(1)$, $s^2 + 2 \beta s + \omega_0^2 = 0$, gives two different values for $s$

\[
s_{\pm} = -\beta \pm\sqrt{\beta^2-\omega_0^2}
\]

Note the use of $2\beta$ in $(1)$ simplifies the above. This gives the general solution as

\[
x(t)= \textrm{e}^{-\beta t}\left[A \textrm{e}^{\sqrt{\beta^2-\omega_0^2}t}+B \textrm{e}^{-\sqrt{\beta^2-\omega_0^2}t}\right]
\]

The size of $\beta$ relative to $\omega_0$ gives rise to four distinct categories of motion.

## Varying $\beta$
### $\beta=0$: No damping

$s_{\pm}$ becomes explicitly complex giving the solution

\[
x(t)= A \textrm{e}^{i\omega_0t}+B \textrm{e}^{-i\omega_0t}\,.
\]

$A$ and $B$ are still complex here. We need to enforce the initial conditions to reveal their form:

\begin{align}
\dot{x}(0)= i\omega_0(a_R + ia_I -b_R - ib_I) &= v \\
\implies a_R &= b_R \\
x(0)= a_R + ia_I + b_R + ib_I &= X_0 \\
\implies a_I &= -b_I
\end{align}

$A$ and $B$ are complex conjugates and can be written in exponential form

\[
A = C\textrm{e}^{i\phi}
\]
\[
B = C\textrm{e}^{-i\phi}
\]

We now have our general solution

\[
x(t) = C \textrm{e}^{i\omega_0t+\phi}+C \textrm{e}^{-i\omega_0t-\phi}\\
& = D\cos(\omega_0t + \phi)
\]

The initial conditions will give us $D$ and $\phi$:

\[
D\cos(\phi) = X_0
\]
\[
-\omega_0D\sin(\phi) = v
\]
\[
\implies\quad D=\sqrt{X_0^2+\frac{v^2}{\omega_0^2}}
\]
\[
\implies\quad \tan\phi=-\frac{v}{\omega_0 X_0}
\]

The above details sinusoidal oscillation as one would expect with no damping term. Setting $v=0$ gives the familiar cosine solution

\[
x(t)=X_0\cos\omega_0t
\]

### $\beta<\omega_0$: Underdamping

In this section we re-introduce a small damping term into $(1)$. We will keep it strictly smaller than $\omega_0$ giving us a new equation of motion:

\[
x(t)= \textrm{e}^{-\beta t}\left[A \textrm{e}^{i\sqrt{\omega_0^2-\beta^2}t}+B \textrm{e}^{-i\sqrt{\omega_0^2-\beta^2}t}\right]
\]

Just as before we enforce the initial conditions to find the form of $A$ and $B$

\[
x(0)= a_R + ia_I + b_R + ib_I = X_0
\]
\[
\implies a_I = -b_I
\]
\[
\dot{x}(0)= -\beta[A+B] +i\sqrt{\omega_0^2-\beta^2}[A-B]=v
\]
\[
-\beta[a_R +ia_I + b_R +ib_I] +i\sqrt{\omega_0^2-\beta^2}[a_R +ia_I - b_R -ib_I]=v
\]
\[
-\beta a_I-\beta b_I + \sqrt{\omega_0^2-\beta^2}(a_R-b_R)=0
\]
\[
\implies a_R = b_R
\]

Despite the damping term we still have $A$ and $B$ as a complex conjugate pair. This gives us, just as in the no damping case, sinusoidal oscillitory motion but with an exponential decay factor:

\[
x(t) = D\textrm{e}^{-\beta t}\cos(\sqrt{\omega_0^2 - \beta^2}\,t + \phi)
\]

Using the initial conditions again we can find $D$ and $\phi$:

\[
D = X_0\sqrt{\frac{(\beta X_0 + v)^2}{(\omega_0^2-\beta^2)X_0^2} + 1}
\]
\[
\tan\phi =-\frac{\beta X_0 + v}{\sqrt{\omega_0^2-\beta^2}X_0}
\]

The proof is left as an exercise (hint: use $\tan^2\theta + 1 = \sec^2\theta$).

### $\beta=\omega_0$: Critical damping
If we were to plug $\beta=\omega_0$ into our general solution we would have exponentials with $0$ in their arguments. This would imply $x(t)=A\textrm{e}^{-\omega_0t}$. In doing so we have over-specified our general solution. Returning to the $(1)$ and subsituting $\beta=\omega_0$ we get

\[
\ddot{x} + 2\omega_0\dot{x} + \omega_0^2x = 0\,.
\]

The manipulations that follow allow us to find a more general solution:

\[
\left(\frac{\textrm{d}^2}{\textrm{d}t^2} + 2\omega_0\frac{\textrm{d}}{\textrm{d}t} + \omega_0^2\right)x=0 \\
\]
\[
\left(\frac{\textrm{d}}{\textrm{d}t}+\omega_0\right)^2x =0 \\
\]
\[
\textrm{let } y = \left(\frac{\textrm{d}}{\textrm{d}t}+\omega_0\right)x
\]
\[
\qquad\implies\qquad \left(\frac{\textrm{d}}{\textrm{d}t}+\omega_0\right)y=0\\
\]
\[
\frac{\textrm{d}y}{\textrm{dt}}=-\omega_0y\\
\]
\[
\frac{\textrm{d}y}{y}=-\omega_0\textrm{d}t\\
\]
\[
y = A\textrm{e}^{-\omega_0t}\\
\]
\[
\left(\frac{\textrm{d}}{\textrm{d}t}+\omega_0\right)x= A\textrm{e}^{-\omega_0t}\\
\]
\[
\frac{\textrm{d}x}{\textrm{d}t}+\omega_0x= A\textrm{e}^{-\omega_0t}\\
\]
\[
\frac{\textrm{d}x}{\textrm{d}t}\textrm{e}^{\omega_0t}+\omega_0x\textrm{e}^{\omega_0t}= A\\
\]
\[
\frac{\textrm{d}}{\textrm{d}t}\left(x\textrm{e}^{\omega_0t}\right)= A\\
\]
\[
x(t)= (At + B)\textrm{e}^{-\omega_0t}
\]

$A$ and $B$ are then simple to find using the initial conditions:

\[
A=v + \omega_0X_0
\]
\[
B=X_0
\]
### $\beta>\omega_0$: Overdamping
