namespace Jdev.JohnGould.Blog.Grpc.Api.Components.Mapping;

using AutoMapper;
using MDParser.Engine.Core;
using engine = MDParser.Engine.Model;
using blog = Models;

public class BlogProfile : Profile
{
    public BlogProfile()
    {
        CreateMap<FreeTextContent, blog.Content>()
            .ForMember(dst => dst.Type, opts => opts.MapFrom(src => src.Type.ToString()));
        CreateMap<engine.H1,blog.BlogChildBase>()
            .ConvertUsing<H1Converter>();
        CreateMap<engine.H2,blog.BlogChildBase>()
            .ConvertUsing<H2Converter>();
        CreateMap<engine.H3,blog.BlogChildBase>()
            .ConvertUsing<H3Converter>();
        CreateMap<engine.Break,blog.BlogChildBase>()
            .ConvertUsing<BreakConverter>();
        CreateMap<engine.P,blog.BlogChildBase>()
            .ConvertUsing<PConverter>();
        CreateMap<engine.DisplayMath,blog.BlogChildBase>()
            .ConvertUsing<DisplayMathConverter>();
        CreateMap<engine.Align,blog.BlogChildBase>()
            .ConvertUsing<AlignConverter>();
    }
}

public class H1Converter : ITypeConverter<engine.H1, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.H1 source, blog.BlogChildBase destination, ResolutionContext context)
    {
        return new blog.FreeText
        {
            Type = "h1",
            Contents = context.Mapper.Map<IEnumerable<blog.Content>>(source.Contents)
        };
    }
}
public class H2Converter : ITypeConverter<engine.H2, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.H2 source, blog.BlogChildBase destination, ResolutionContext context)
    {
        return new blog.FreeText
        {
            Type = "h2",
            Contents = context.Mapper.Map<IEnumerable<blog.Content>>(source.Contents)
        };
    }
}
public class H3Converter : ITypeConverter<engine.H3, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.H3 source, blog.BlogChildBase destination, ResolutionContext context)
    {
        return new blog.FreeText
        {
            Type = "h3",
            Contents = context.Mapper.Map<IEnumerable<blog.Content>>(source.Contents)
        };
    }
}
public class BreakConverter : ITypeConverter<engine.Break, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.Break source, blog.BlogChildBase destination, ResolutionContext context)
    {
        return new blog.Break()
        {
            Type = "break"
        };
    }
}
public class PConverter : ITypeConverter<engine.P, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.P source, blog.BlogChildBase destination, ResolutionContext context)
    {
        return new blog.FreeText
        {
            Type = "p",
            Contents = context.Mapper.Map<IEnumerable<blog.Content>>(source.Contents)
        };
    }
}
public class DisplayMathConverter : ITypeConverter<engine.DisplayMath, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.DisplayMath source, blog.BlogChildBase destination,
        ResolutionContext context)
    {
        return new blog.Math()
        {
            Type = "displaymath",
            Content = source.Content
        };
    }
}
public class AlignConverter : ITypeConverter<engine.Align, blog.BlogChildBase>
{
    public blog.BlogChildBase Convert(engine.Align source, blog.BlogChildBase destination,
        ResolutionContext context)
    {
        return new blog.Math()
        {
            Type = "align",
            Content = source.Content
        };
    }
}
