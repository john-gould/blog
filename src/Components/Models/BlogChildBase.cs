namespace Jdev.JohnGould.Blog.Grpc.Api.Components.Models;

using System.Text.Json.Serialization;

[JsonDerivedType(typeof(FreeText))]
[JsonDerivedType(typeof(Math))]
[JsonDerivedType(typeof(Break))]
public class BlogChildBase
{
    public string Type { get; set; } = default!;
}

public class FreeText : BlogChildBase
{
    public IEnumerable<Content> Contents { get; set; } = default!;
}

public class Math : BlogChildBase
{
    public string Content { get; set; } = default!;
}
public class Break : BlogChildBase {}

public class Content
{
    public string Text { get; set; } = default!;
    public string Type { get; set; } = default!;
}